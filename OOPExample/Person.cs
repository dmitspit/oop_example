﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOPExample
{
    class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Password { get; set; }

        public string path = @"Data\TestData.csv";
        public List<Person> GetPersonFromCSVFile(string path) 
        {
            string[] lines = File.ReadAllLines(path);
            List<Person> list = new List<Person>();
            foreach (var line in lines)
            {
                string[] values = line.Split(',');
                var user = new Person() { FirstName = values[0], LastName = values[1], Password = values[2] };
                list.Add(user);
            }
            return list;
        }

        public static IEnumerable<Person> GetPersonFromCSVFile1()
        {
            string path = @"Data\TestData.csv";
            string[] lines = File.ReadAllLines(path);
            foreach (var line in lines)
            {
                string[] values = line.Split(',');
                yield return new Person() { FirstName = values[0], LastName = values[1], Password = values[2] };
            }

        }
    }   
}
