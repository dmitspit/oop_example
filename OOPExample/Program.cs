﻿using System;
using System.Collections.Generic;
using System.IO;

namespace OOPExample
{
    class Program
    {
        static void Main(string[] args)
        {

        }

        public interface ISeries
        {
            int GetNext(); // возвратить следующее по порядку число
            void Reset(); // перезапустить
            void SetStart(int х); // задать начальное значение
        }
        public interface IA 
        {
            string Name { get; set; }
        }
    }
}
