﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InterfaceSegregation
{
    class Program
    {
        static void Main(string[] args)
        {


        }

        public interface ICar
        {
            void Start();
            void Stop();
            void ChangeOil();
            void AddFuel();
        }

        public interface IElectricCar 
        {
            void Start();
            void Stop();
            void ChangeBattery();
            void ChargeBattery();
        }



        public class Truck : ICar
        {
            public void AddFuel()
            {
                throw new NotImplementedException();
            }

            public void ChangeOil()
            {
                throw new NotImplementedException();
            }

            public void Start()
            {
                throw new NotImplementedException();
            }

            public void Stop()
            {
                throw new NotImplementedException();
            }
        }

        public class ElectricCar : ICar
        {
            public void AddFuel()
            {
                throw new NotImplementedException();
            }

            public void ChangeOil()
            {
                throw new NotImplementedException();
            }

            public void Start()
            {
                throw new NotImplementedException();
            }

            public void Stop()
            {
                throw new NotImplementedException();
            }
        }
    }
}
