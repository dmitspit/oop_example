﻿
namespace PatternFacade
{
    class SportCar
    {
        public Car sportCar = new Car();
        
        public void CreateSportCar(Facade facade) 
        {
            facade.PrepareCar(sportCar);
        }
    
    }

}
