﻿using System;
using System.Collections.Generic;


namespace PatternFacade
{
    class Facade
    {
        protected Engine engine = new Engine();
        protected SparePart sparePart = new SparePart();
        public void PrepareCar(Car car) 
        {
            car.Engine = engine.BuildEngineV10();
            car.StaringWheel = sparePart.GetSportWheel();
        }
    }
}
