﻿
namespace PatternFacade
{
    class Program
    {
        static void Main(string[] args)
        {
            Facade facade = new Facade();
            SportCar AudiR8 = new SportCar();
            AudiR8.CreateSportCar(facade);
        }
    }
}
