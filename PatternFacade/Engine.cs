﻿

namespace PatternFacade
{
    public class Engine
    {
        public string BuildEngineV10()
        {
            return "700 Hp";
        }
        public string BuildEngineV8()
        {
            return "500 Hp";
        }
        public string BuildEngineV6()
        {
            return "300 Hp";
        }
    }
}