﻿namespace PatternFacade
{
    public class SparePart
    {
        public bool GetSportWheel()
        {
            return true;
        }
        public int GetDoor()
        {
            return 4;
        }
    }
}