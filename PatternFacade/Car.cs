﻿
namespace PatternFacade
{
    public class Car
    {
        public string Engine { get; set; }
        public bool StaringWheel { get; set ; }
    }
}