﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInversion
{
    class Program
    {
        static void Main(string[] args)
        {
            Notification notification = new Notification( new Email());  
        }

        public interface IMessenger
        {
            void Send();
        }

        public class Email : IMessenger
        {
            public void Send()
            {
            }
        }

        public class SMS : IMessenger
        {
            public void Send()
            {
              
            }
        }

        public class Notification
        {
            private IMessenger _messenger;
            public Notification(IMessenger messenger)
            {
                _messenger = messenger;
            }

            public void DoNotify()
            {
                _messenger.Send();
            }
        }
    }
}
