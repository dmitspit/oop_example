﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace NewProject
{
    class Program
    {

        static void Main(string[] args)
        {

            int[] integers = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            int result12 = Sum(integers, x => x > 5);
            Console.WriteLine(result12); // 30

            int result2 = Sum(integers, x => x % 2 == 0);
            Console.WriteLine(result2);  //20

            int Sum(int[] numbers, IsEqual func)
            {
                int result = 0;
                foreach (int i in numbers)
                {
                    if (func(i))
                        result += i;
                }
                return result;
            }
            
            Console.Read();
        }
        delegate bool IsEqual(int x);

    }
}
