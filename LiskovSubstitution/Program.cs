﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiskovSubstitution
{
    class Program
    {
        static public int Area(Rectangle r) => r.Width * r.Height;
        static void Main(string[] args)
        {
            //Rectangle rc = new Rectangle(2,3);
            //Console.WriteLine("The area is " + Area(rc));
            //Console.ReadLine();

            //Sqare sq = new Sqare();
            //sq.Width = 4;
            //Console.WriteLine("The area is " + Area(sq));
            //Console.ReadLine();

            Rectangle newSq = new Sqare();
            newSq.Width = 4;
            Console.WriteLine("\n The area is  "+ Area(newSq));
            Console.ReadLine();
        }
    }
    public class Rectangle
    {
        public virtual int Width { get; set; }
        public virtual int Height { get; set; }
        public Rectangle() { }

        public Rectangle(int width, int height) 
        {
            Width = width;
            Height = height;
        }
    }
    public class Sqare : Rectangle
    {
        public override int Width
        {
            set { base.Width = base.Height = value; }
        }
        public override int Height 
        {
            set { base.Width = base.Height = value; }
        }
    }
}
