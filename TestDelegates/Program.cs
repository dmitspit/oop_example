﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestDelegates
{
    class Program
    {
        public delegate int Operation(int x, int b);
        static void Main(string[] args)
        {
            Operation op = new Operation(Practice.SumStat);
            op += Practice.Sum;
            int res = op.Invoke(5,2);
            
            Console.WriteLine(res);
            Console.ReadLine();
            //Math mat = new Math();
            //Operation op = mat.First;
            //int res = op(1, 2);
        }

    }
}
