﻿using PatternChainOfResponsibility_;
using System;
using System.Collections.Generic;

namespace PatternChainOfResponsibility_
{
    class Program
    {
        private static object driver;

        static void Main(string[] args)
        {
            Person user = new Person();
            user.payByPayPal = true;

            var orderPage = new OrderPage(driver);
            var payPalPage = new PayPalPage(driver);
            var privatBankPage = new PrivatBankPage(driver);

            orderPage.SetNext(payPalPage).SetNext(privatBankPage);

            Client.ClientCode(orderPage, user);

        }
    }

    public interface IHandler
    {
        IHandler SetNext(IHandler handler);

        object Handle(Person person);
    }
    abstract class AbstractHandler : IHandler
    {
        private IHandler _nextHandler;

        public IHandler SetNext(IHandler handler)
        {
            this._nextHandler = handler;
            return handler;
        }

        public virtual object Handle(Person person)
        {
            if (this._nextHandler != null)
            {
                return this._nextHandler.Handle(person);
            }
            else
            {
                return null;
            }
        }
    }

    class OrderPage : AbstractHandler
    {
        public override object Handle(Person person)
        {
            if (person.payByCash == true)
            {
                MakeOrder();
                return new HomePage(driver);
            }
            else
            {
                return base.Handle(person);
            }
        }
        private void MakeOrder()
        {
            throw new NotImplementedException();
        }
    }

    class PayPalPage : AbstractHandler
    {

        public override object Handle(Person person)
        {
            if (person.payByPayPal == true)
            {
                PayOrderByPayPal();
                return new HomePage(driver);
            }
            else
            {
                return base.Handle(person);
            }
        }

        private void PayOrderByPayPal()
        {
            throw new NotImplementedException();
        }
        private object driver;
        public PayPalPage(object driver)
        {
            this.driver = driver;
        }

    }

    class PrivatBankPage : AbstractHandler
    {
        public override object Handle(Person person)
        {
            if (person.payByPrivateBank == true)
            {
                PayOrderByPrivateBank();
                return new HomePage(driver);
            }
            else
            {
                return base.Handle(person);
            }
        }
        private void PayOrderByPrivateBank()
        {
            throw new NotImplementedException();
        }

        private object driver;

        public PrivatBankPage(object driver)
        {
            this.driver = driver;
        }
    }
 }

    class Client
    {
        public static void ClientCode(AbstractHandler handler, Person person)
        {
            handler.Handle(person);
        }
    }
