﻿namespace PatternChainOfResponsibility_
{
    public class Person
    {
        internal bool payByCash;
        internal bool payByPayPal;
        internal bool payByPrivateBank;
    }
}