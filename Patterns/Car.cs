﻿using System;
using System.Collections.Generic;

namespace Patterns
{
    public class Car
    {
        private int seats;
        private string engine;
        public string SetEngile { set { engine = value; } }
        public int SetSeats { set { seats = value;}}
        private List<Car> parts = new List<Car>();
        public void Add(Car part)
        {
            parts.Add(part);
        }
        public void Show()
        {
            Console.WriteLine("\nCar Parts:");
            foreach (Car part in parts)
                Console.WriteLine($"seats =>{part.seats}\n type of engine => {part.engine}");
        }
    }

}
