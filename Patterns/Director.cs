﻿
namespace Patterns
{
    class Director
    {
        private ICarBuilder _builder;
        public ICarBuilder Builder{ set { _builder = value; }}
        public void BuildMinimalCar()
        {
            this._builder.SetEngine();
        }
        public void BuilMaxCar() 
        {
            this._builder.SetEngine();
            this._builder.SetSeats();
        }
    }
}
