﻿namespace Patterns
{
    public class ConcreteBuilder : ICarBuilder
    {
        private Car car = new Car();
        public void Reset()
        {
            this.car = new Car();
        }
        public void SetEngine()
        {
            this.car.SetEngile = "2.6 turbo";
            this.car.Add(car);
        }
        public void SetSeats()
        {
            this.car.SetSeats = 2;
            this.car.Add(car);
        }
        public Car GetSportCar() 
        {
            Car carResult = this.car;
            this.Reset();
            return carResult;
        }

    }
}
