﻿

namespace Patterns
{
    interface ICarBuilder
    {
        void Reset();
        void SetSeats();
        void SetEngine();
    }
}
