﻿
namespace Patterns
{
    class Program
    {
        static void Main(string[] args)
        {
            var builderSpotrCar = new ConcreteBuilder();
            builderSpotrCar.SetEngine();
            builderSpotrCar.SetSeats();
        }
    }
}
